$(document).ready(function() {

    // SCROLL TOP BAR
    $('.top-bar a').click(function(event) {
        event.preventDefault();

        var hash = this.hash;

        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 800);
    });


    // VÍDEOS LIGHTBOX
    $('.video-lightbox').fancybox({
        padding: 0,
        type: 'iframe',
        width: 800,
        height: 450,
        aspectRatio: true
    });


    // DEPOIMENTOS
    $('.depoimentos-cycle').cycle({
        slides: '>.depoimento',
        pagerTemplate: '<a href=#>{{slideNum}}</a>',
        autoHeight: 'calc',
        timeout: 7000
    });

});
